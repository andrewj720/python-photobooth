# Python Photobooth

Code to run a DIY photobooth, using a Raspeberry Pi, a Pi camera module, a monitor to display user instructions/camera output/countdown timer, internet access to a predefined Google Photos album, and a big button for users to press to start.